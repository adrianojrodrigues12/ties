<!DOCTYPE html>
<html>
<head>

<?php

$title = "T & C - Gravataria";

?>

<title><?=$title ?> </title>

<link rel="shortcut icon" href="favicon.ico" >

<meta charset="utf-8" >

<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta http-equiv="Content-Type" content="text/html; charset=ISAO-8859-1" >
<meta http-equiv="Content-Language" content="PT-BR" >
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="author" content="Adriano de Jesus Rodrigues" >
<meta name="language" content="portuguese" >

<link href="https://fonts.googleapis.com/css?family=Cinzel:400,700&amp;subset=latin-ext" rel="stylesheet">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<link rel="stylesheet" href="_css/style.css" >


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


</head>
<body>
    <div class="container-fluid no-padding">
        <?php 
            include ("paginas/01-topo.php");
        ?>
    </div>
    <?php
        include ("paginas/02-banner.php");
        include ("paginas/03-destaque.php");
        include ("paginas/04-institucional.php");
        include ("paginas/05-contato.php");
        //include ("paginas/06-rodape.php");
    ?>
</body>
</html>