<div class="col-lg-12 no-padding">
    <div class="itens-topo">
        <nav class="nav navbar bg-light">
            <a href="#" class="navbar navbar-brand">
                <img src="_source/images/tec_logo.png" style="width:50px;" alt="T&C Logo">
                &nbsp;&nbsp;Ties & Company
            </a>    
            <div class="links pull-right">
                <a href="#" class="a link btn btn-outline-info btn-sm">
                    <i class="fas fa-home"></i>&nbsp;
                    Página Inicial
                </a>
                <a href="#" class="a link btn btn-outline-info btn-sm">
                    <i class="fas fa-tags"></i>&nbsp;
                    Produtos
                </a>
                <a href="#" class="a link btn btn-outline-info btn-sm">
                    <i class="fas fa-headset"></i>&nbsp;
                    Contato
                </a>
            </div>
        </nav>
    </div>
</div>