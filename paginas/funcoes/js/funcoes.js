// Funcao de alerta para envio de formulario
function alerta(link, mensagem, tipo) {
	
	noty({
		
		text: mensagem,
		type: tipo ? tipo : 'alert',
		buttons: [{
			
			addClass: 'btn btn-primary', text: 'OK', onClick: function($noty) {
				
				window.location = link;
			}
		}]
	});
	
}