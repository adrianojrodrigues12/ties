<div class="destaque container col-lg-12">
    <div class="subtitulo display-4">
        Produtos em destaque
    </div>
</div>
<div class="produto-lista">
    <div class="produto-destaque">
        <div class="produto-desconto">
            <span class="badge badge-success">10% à vista!</span>
        </div>
        <div class="produto-imagem">
            <img src="_source/images/produto1.jpg" alt="Produto-01" class="produto-img responsive">
        </div>
        <div class="produto-titulo">
            Gravata Cinza + Abotuadura
        </div>
        <div class="produto-valor">
            R$ 79,90
        </div>
        <div class="botao-comprar">
            <?php
                $modelo = "Gravata Cinza + Abotuadura";
                include("99-whats.php");
            ?>
        </div>
    </div>
    <div class="produto-destaque">
        <div class="produto-imagem">
            <img src="_source/images/produto6.jpg" alt="Produto-01" class="produto-img responsive">
        </div>
        <div class="produto-titulo">
            Gravata Azul e Preto
        </div>
        <div class="produto-valor">
            R$ 39,90
        </div>
        <div class="botao-comprar">
            <?php
                $modelo = "Gravata Azul e Preto";
                include("99-whats.php");
            ?>
        </div>
    </div>
    <div class="produto-destaque">
        <div class="produto-imagem">
            <img src="_source/images/produto7.jpg" alt="Produto-01" class="produto-img responsive">
        </div>
        <div class="produto-titulo">
            Gravata Flores + Abotuadura
        </div>
        <div class="produto-valor">
            R$ 49,90
        </div>
        <div class="botao-comprar">
            <?php
                $modelo = "Gravata Flores + Abotuadura";
                include("99-whats.php");
            ?>
        </div>
    </div>
</div>