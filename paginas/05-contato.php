<?php

?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

<div class="subtitulo display-5 col-lg-12 text-center" style="padding:5px;">
    Entre em contato conosco
</div>

<div class="form-contato col-lg-6 col-lg-offset-3">
    <form action="paginas/05-1-email-contato.php" method="post">
        <div class="row">
            <div class="form-group" style="margin-right:20px;">
                <label for="nomeContato">Nome</label>
                <input type="text" class="form-control" id="nomeContato" name="nomeContato" required placeholder="Digite seu nome...">
            </div>
            <div class="form-group">
                <label for="sobrenomeContato">Sobrenome</label>
                <input type="text" class="form-control" id="sobrenomeContato" name="sobrenomeContato" required placeholder="Digite seu Sobrenome...">
            </div>
        </div>
        <div class="row">
            <div class="form-group" style="margin-right:20px;">
                <label for="emailContato">e-mail</label>
                <input type="email" class="form-control" id="emailContato" name="emailContato" required placeholder="Digite seu e-mail...">
            </div>
            <div class="form-group">
                <label for="telefone">Telefone</label>
                <input type="phone" class="form-control telefone" id="telefone" name="telefone" placeholder="(##) #####-####" required >
            </div>
        </div>
        <div class="form-group">
            <label for="mensagemContato">Mensagem</label>
            <input type="textarea" class="form-control" id="mensagemContato" name="mensagemContato" placeholder="Digite alguma mensagem..." maxlength="500" required>
        </div>
        <button type="submit" name="submit" class="btn btn-primary">Enviar&nbsp;<i class="far fa-paper-plane"></i></button>
    </form>
</div>

<script>
    jQuery("#telefone")
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
        });
</script>