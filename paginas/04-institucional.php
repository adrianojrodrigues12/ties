<div class="destaque container col-lg-12">
    <div class="intitucional col-lg-12 fundo-institucional">
        <div class="subtitulo display-4 col-lg-12 text-center" style="padding:5px;">
            Institucional
        </div>
        <div class="texto-institucional text-center col-lg-6 offset-lg-3">
            <p>
                A <strong>Tie's & Company</strong> é uma empresa que surgiu da necessidade de levar estilo, elegância e bom gosto ao homem moderno, tudo isso com bom preço e excelente qualidade nos produtos e atendimento.
            </p>
            <p>
                Com uma linha de acessórios completa com o que há de melhor na moda masculina, a  T&C trás até você a oportunidade de mudar seu estilo e apresentação, obtendo destaque com elegância, e ainda assim, sem abrir mão do bom gosto e simplicidade.
            </p>
        </div>
    </div>
</div>