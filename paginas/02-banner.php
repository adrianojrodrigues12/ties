<div class="banner row">
    <div class="container-fluid no-padding">
        <div id="bannerm" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#bannerm" data-slide-to="0" class="active"></li>
                <li data-target="#bannerm" data-slide-to="1"></li>
                <li data-target="#bannerm" data-slide-to="2"></li>
                <li data-target="#bannerm" data-slide-to="3"></li>
                <li data-target="#bannerm" data-slide-to="4"></li>
            </ul>
            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="_source/images/banner1.jpg" alt="Banner" style="width:100%;">
                </div>
                <div class="carousel-item">
                    <img src="_source/images/banner2.jpg" alt="Banner" style="width:100%;">
                </div>
                <div class="carousel-item">
                    <img src="_source/images/banner3.jpg" alt="Banner" style="width:100%;">
                </div>
                <div class="carousel-item">
                    <img src="_source/images/banner4.jpg" alt="Banner" style="width:100%;">
                </div>
                <div class="carousel-item">
                    <img src="_source/images/banner5.jpg" alt="Banner" style="width:100%;">
                </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#bannerm" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#bannerm" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    </div>
</div>